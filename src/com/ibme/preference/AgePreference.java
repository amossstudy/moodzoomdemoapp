// Copyright (c) 2014, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Based on code from http://stackoverflow.com/a/10608622/2433501

package com.ibme.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;

public class AgePreference extends DialogPreference {
    private NumberPicker picker = null;

    private int currentValue;
    
    private int minAge;
    private int maxAge;
    
    String[] displayValues;
    
    public AgePreference(Context ctxt) {
        this(ctxt, null);
    }

    public AgePreference(Context ctxt, AttributeSet attrs) {
        this(ctxt, attrs, 0);
    }

    public AgePreference(Context ctxt, AttributeSet attrs, int defStyle) {
        super(ctxt, attrs, defStyle);

        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);
        
        minAge = 16;
        maxAge = 100;
        
        displayValues = new String[maxAge - minAge + 3];
        displayValues[0] = "Age not given";
        for (int i = 0; i <= (maxAge - minAge); i++) {
        	displayValues[i + 1] = Integer.toString(minAge + i);
        }
        displayValues[maxAge - minAge + 2] = Integer.toString(maxAge + 1) + "+";
        
        currentValue = minAge - 1;
    }

    @Override
    protected View onCreateDialogView() {
        picker = new NumberPicker(getContext());
        picker.setMinValue(minAge - 1);
        picker.setMaxValue(maxAge + 1);
        picker.setWrapSelectorWheel(false);
        picker.setDisplayedValues(displayValues);
        return (picker);
    }

    @Override
    protected void onBindDialogView(View v) {
        super.onBindDialogView(v);
        picker.setValue(currentValue);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {
        	currentValue = picker.getValue();

            setSummary(getSummary());
            if (callChangeListener(currentValue)) {
            	if (currentValue < minAge) {
            		persistInt(-1);
            	} else {
            		persistInt(currentValue);
            	}
                notifyChanged();
            }
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return (a.getString(index));
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
    	if (restoreValue) {
            if (defaultValue == null) {
            	currentValue = getPersistedInt(minAge - 1);
            } else {
            	currentValue = Integer.parseInt(getPersistedString((String) defaultValue));
            }
        } else {
            if (defaultValue == null) {
            	currentValue = -1;
            } else {
            	currentValue = Integer.parseInt((String) defaultValue);
            }
            if (currentValue < minAge) {
            	currentValue = -1;
            } else if (currentValue > maxAge) {
            	currentValue = maxAge + 1;
            }
            persistInt(currentValue);
        }
        
        if (currentValue < (minAge - 1)) {
        	currentValue = minAge - 1;
        } else if (currentValue > (maxAge + 1)) {
        	currentValue = maxAge + 1;
        }
    }
    
    public String getDisplayValue() {
    	return displayValues[currentValue - minAge + 1];
    }
    
    public int getValue() {
    	if (currentValue >= minAge) {
    		return currentValue;
    	} else {
    		return -1;
    	}
    }
}

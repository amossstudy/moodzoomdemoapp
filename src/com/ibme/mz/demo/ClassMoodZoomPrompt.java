// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Originally written by Maxim Osipov

package com.ibme.mz.demo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

public class ClassMoodZoomPrompt {

	private static final String TAG = ClassMoodZoomPrompt.class.getSimpleName();

	private Context mContext;
	private File mFile;
	private BufferedWriter mWriter;
	private long mTsPrev;

	public ClassMoodZoomPrompt(Context context)
	{
		mContext = context;
	}

	// Open for write
	public void init(long ts)
	{
		// TODO: Fix it to check and wait for storage
		try {
			File root = Environment.getExternalStorageDirectory();
			if (root.canWrite()){
				// Create folders
				File folder = new File(root, ClassConsts.FILES_ROOT);
				if (!folder.exists()) {
					folder.mkdirs();
				}
				// Initialise the main calls & texts file
				SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
				fmt.setTimeZone(TimeZone.getTimeZone("UTC"));
				mFile = new File(folder, "mood-zoom_prompt-" + fmt.format(new Date(ts)) + ".csv");
				mWriter = new BufferedWriter(new FileWriter(mFile, true));
				
				File[] files = folder.listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return (name.toLowerCase().matches("^mood-zoom_prompt-.*csv$") &&
								!name.toLowerCase().equals(mFile.getName().toLowerCase()));
					}
				});
				if (files.length > 0) {
					TaskZipper tz = new TaskZipper();
					tz.setUploadOnCompletion(mContext, true);
					tz.execute(files);
				}
			} else {
				Log.e(TAG, "SD card not writable");
			}
		} catch (Exception e) {
			Log.e(TAG, "Could not open file " + e.getMessage());
		}
		mTsPrev = ts;
	}

	public void fini()
	{
		try {
			if (mWriter != null) {
				mWriter.close();
				mWriter = null;
			}
			if (mFile != null) {
				TaskZipper tz = new TaskZipper();
				tz.setUploadOnCompletion(mContext, true);
				tz.execute(mFile);
				mFile = null;
			}
		} catch (Exception e) {
			Log.e(TAG, "Could not close file " + e.getMessage());
		}
	}

	void update(Context ctx, long ts, long lMoodZoomType)
	{
		long today = ts/ClassConsts.MILLIDAY;
		long yesterday = mTsPrev/ClassConsts.MILLIDAY;
		if (today > yesterday) {
			File f = mFile;
			fini();
			TaskZipper tz = new TaskZipper();
			tz.setUploadOnCompletion(mContext, true);
			tz.execute(f);
			init(ts);
		}
		if (mWriter != null) {
			try {
				SimpleDateFormat fmt = new SimpleDateFormat(ClassConsts.DATEFMT);
				String str = fmt.format(new Date(ts)) + "," + Long.toString(lMoodZoomType) + "\n";
				mWriter.write(str);
				mWriter.flush();
			} catch(Exception e) {
				Log.e(TAG, "Could not write file " + e.getMessage());
			}
		}
		mTsPrev = ts;
	}
}

// Copyright (c) 2014, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

package com.ibme.mz.demo;

import com.ibme.mz.demo.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.preference.PreferenceManager;
import android.util.Log;

public class VersionManager {
	
	private static final String TAG = VersionManager.class.getSimpleName();
	
	public static void checkForUpgrades(Context context) {
		checkForUpgrades(context, false);
	}
	public static void checkForUpgrades(Context context, boolean bUploadSettings) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		
		// For debug use only!
		//editor = settings.edit();
		//editor.clear();
		//editor.commit();
		//editor = null;
		
		// Initialize preferences
		PreferenceManager.setDefaultValues(context, R.xml.preferences, true);
		PreferenceManager.setDefaultValues(context, R.xml.preferences_welcome_setup_1, true);
		PreferenceManager.setDefaultValues(context, R.xml.preferences_welcome_setup_3, true);
		
		int iPreviousVersion = prefs.getInt("currentVersion", 0);
		int iCurrentVersion = 0;
		
		PackageInfo pinfo;
		try {
			pinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			iCurrentVersion = pinfo.versionCode;
		} catch (NameNotFoundException e) {
			// meh
		}
		
		// For debug use only!
		//iCurrentVersion = 1019;
		//iPreviousVersion = 1018;

		Log.i(TAG, "Previous Version: " + iPreviousVersion);
		Log.i(TAG, "Current Version: " + iCurrentVersion);
		
		SharedPreferences.Editor editor = prefs.edit();
		if ((iCurrentVersion != iPreviousVersion) && (iPreviousVersion != 0)) {
			Log.i(TAG, "Upgrading from version " + iPreviousVersion + " to " + iCurrentVersion);
		} else if (iPreviousVersion == 0) {
			Log.i(TAG, "Fresh install of version " + iCurrentVersion);
			
			editor.putLong("dateInstalled", System.currentTimeMillis());
		}
		
		editor.putInt("currentVersion", iCurrentVersion);
		editor.commit();
	}
}

// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Original version by Maxim Osipov
// Forked from https://github.com/maximosipov/actopsy, commit id faa1a49996

package com.ibme.mz.demo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.preference.PreferenceManager;
import android.util.Log;

// In order to start the service the application should run from the phone memory 
public class ServiceCollect extends Service implements
	SensorEventListener, OnSharedPreferenceChangeListener {

	// TODO: Make it a foreground service

	private static final String TAG = ServiceCollect.class.getSimpleName();

	private SensorManager mSensorManager;
	private Sensor mSensorAccelerometer;

	private ClassProfileAccelerometry mProfile;
	
	@Override
	public void onCreate() {
		// TODO: Are we actually running in this thread???
		HandlerThread thread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		int sdelay = SensorManager.SENSOR_DELAY_NORMAL;
		
		long ts = System.currentTimeMillis();
		
		mProfile = new ClassProfileAccelerometry(this);
		mProfile.init(ts);

		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		if (mSensorAccelerometer != null) {
			mSensorManager.registerListener(this, mSensorAccelerometer, sdelay);
		}
		
		CreateMoodZoomSchedule(this, prefs);
		ScheduleMoodZoom(this);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i(TAG, "ServiceCollect started");

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		prefs.registerOnSharedPreferenceChangeListener(this);
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		mSensorManager.unregisterListener(this);
		
		mProfile.fini();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		prefs.unregisterOnSharedPreferenceChangeListener(this);

		Log.i(TAG, "ServiceCollect destroyed");
	}

	///////////////////////////////////////////////////////////////////////////
	// Messages interface (unused)
	///////////////////////////////////////////////////////////////////////////
	public static final int MSG_REGISTER_CLIENT = 1;
	public static final int MSG_UNREGISTER_CLIENT = 2;

	private ArrayList<Messenger> mClients = new ArrayList<Messenger>();
	final Messenger mMessenger = new Messenger(new IncomingHandler());

	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}

	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_REGISTER_CLIENT:
				mClients.add(msg.replyTo);
				break;
			case MSG_UNREGISTER_CLIENT:
				mClients.remove(msg.replyTo);
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// Sensors interface
	///////////////////////////////////////////////////////////////////////////
	@Override
	public void onSensorChanged(SensorEvent event) {
		// event.timestamp has a different meaning on different android versions
		long ts = System.currentTimeMillis();

		int type = event.sensor.getType();
		if (type == Sensor.TYPE_ACCELEROMETER) {
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];
			mProfile.update(ts, x, y, z);
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO: What's that?
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		if (key.equals("checkboxMoodZoom") || key.equals("timeMoodZoomStart") || 
				key.equals("timeMoodZoomEnd") || key.equals("listEvent")) {
			CreateMoodZoomSchedule(this, prefs);
			ScheduleMoodZoom(this);
		}
	}

	public static void CreateMoodZoomSchedule(Context context, SharedPreferences prefs) {
		boolean bMZ_Enabled = prefs.getBoolean("checkboxMoodZoom", true);
		long lMZ_Start = prefs.getLong("timeMoodZoomStart", 32400000);
		long lMZ_End = prefs.getLong("timeMoodZoomEnd", 75600000);
		
		SharedPreferences schedule = context.getSharedPreferences(
		        ClassConsts.MOOD_ZOOM_SCHEDULE, Context.MODE_PRIVATE);
		
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int eventId = Integer.parseInt(prefs.getString("listEvent", Integer.toString(context.getResources().getInteger(R.integer.event_none))));

		Calendar dNow = Calendar.getInstance();
		Calendar cNow = Calendar.getInstance();
		cNow.clear();
		cNow.set(dNow.get(Calendar.YEAR), dNow.get(Calendar.MONTH), dNow.get(Calendar.DAY_OF_MONTH));
		
		SharedPreferences.Editor editor = schedule.edit();
		Log.i(TAG + "::CreateMoodZoomSchedule", "Time now: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss+zzz").format(dNow.getTime()) + " (" + Long.toString(dNow.getTimeInMillis()) + ")");
		if (bMZ_Enabled) {
			editor.putLong("typeMoodZoom", 2); // 2: high intensity
			
			int numberOfPrompts;
			
			long[] lScheduleTime;
			
			if (eventId == context.getResources().getInteger(R.integer.event_bigspd)) {
				int[] iScheduleTime = context.getResources().getIntArray(R.array.event_bigspd_mood_zoom_schedule);
				numberOfPrompts = iScheduleTime.length;
				lScheduleTime = new long[numberOfPrompts];
				
				for (int i = 0; i < numberOfPrompts; i++) {
					lScheduleTime[i] = (long)iScheduleTime[i] * 1000l; // Convert seconds to milliseconds.
				}
			} else {
				numberOfPrompts = 10;
				lScheduleTime = new long[numberOfPrompts];
				
				long lDiff = lMZ_End - lMZ_Start;
				double dDiff = (double)lDiff / (numberOfPrompts - 1);
				
				for (int i = 0; i < numberOfPrompts; i++) {
					lScheduleTime[i] = Math.round(lMZ_Start + (i * dDiff));
				}
			}
			
			Calendar[] cScheduleTime = new Calendar[numberOfPrompts];
			
			for (int i = 0; i < numberOfPrompts; i++) {
				cScheduleTime[i] = Calendar.getInstance();
				cScheduleTime[i].setTimeInMillis(cNow.getTimeInMillis() + lScheduleTime[i]);
			}
			
			for (int i = 0; i < numberOfPrompts; i++) {
				if (!cScheduleTime[numberOfPrompts - 1].after(dNow)) {
					cScheduleTime[i].add(Calendar.DATE, 1);
				}
				editor.putLong("dateSchedule" + Integer.toString(i + 1), cScheduleTime[i].getTimeInMillis());
				Log.i(TAG + "::CreateMoodZoomSchedule", "Added schedule: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss+zzz").format(cScheduleTime[i].getTime()));
			}
		} else {
			editor.putLong("typeMoodZoom", 0); // 0: disabled
		}
		
		editor.commit();
	}
	
	public static void ScheduleMoodZoom(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		
		SharedPreferences schedule = context.getSharedPreferences(
		        ClassConsts.MOOD_ZOOM_SCHEDULE, Context.MODE_PRIVATE);
		
		long lMoodZoomType = schedule.getLong("typeMoodZoom", 0); // 0: disabled, 1: daily, 2: high intensity
		
		Log.i(TAG + "::ScheduleMoodZoom", "Stopping alarm.");
		AlarmManager a = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, ReceiverMoodZoom.class);
		PendingIntent pending = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_NO_CREATE);
		if (pending != null) {
			a.cancel(pending);
		}

		long lSchedule = 0;
		
		if (lMoodZoomType == 0) {
			SharedPreferences.Editor editor = prefs.edit();
			editor.putLong("nextNoodZoomPrompt", 0);
			editor.commit();
			
			return;
		} else if (lMoodZoomType == 2) {
			Calendar cSchedule = Calendar.getInstance();
			for (int i = 0; i < 10; i++) {
				lSchedule = schedule.getLong("dateSchedule" + Integer.toString(i + 1), 0);
				cSchedule.setTimeInMillis(lSchedule);
				
				if (cSchedule.after(Calendar.getInstance())) {
					break;
				}
				lSchedule = 0;
			}
		}
		
		if (lSchedule == 0) {
			Log.i(TAG + "::ScheduleMoodZoom", "No valid schedule found. Rescheduling.");
			CreateMoodZoomSchedule(context, prefs);
			ScheduleMoodZoom(context);
			return;
		}
		
		pending = PendingIntent.getBroadcast(context, 0, intent, 0);
		
		Log.i(TAG + "::ScheduleMoodZoom", "Scheduling alarm: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss+zzz").format(new Date(lSchedule)));
		a.set(AlarmManager.RTC_WAKEUP, lSchedule, pending);
		
		SharedPreferences.Editor editor = prefs.edit();
		editor.putLong("nextNoodZoomPrompt", lSchedule);
		editor.commit();
	}
}
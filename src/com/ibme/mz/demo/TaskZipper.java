// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Original version by Maxim Osipov
// Forked from https://github.com/maximosipov/actopsy, commit id faa1a49996

package com.ibme.mz.demo;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry; 
import java.util.zip.ZipOutputStream; 

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

public class TaskZipper extends AsyncTask<File, TaskZipper.ZipperProgress, File[]> {

	class ZipperProgress {
		private String file;
		private String status;
		
		public ZipperProgress(File file, String status) {
			this.file = file.getName();
			this.status = status;
		}
		
		@Override
		public String toString() {
			return "Zipping " + file + " " + status;
		}
	}
	
	private static final String TAG = TaskZipper.class.getSimpleName();
	
	private String outDir;
	private String appendToFilename;
	private String statusUpdatePreference;
	private Context context;
	private boolean uploadOnCompletion;
	private boolean deleteOnCompletion;
	
	public TaskZipper() {
		outDir = "";
		appendToFilename = "";
		statusUpdatePreference = "";
		context = null;
		uploadOnCompletion = false;
		deleteOnCompletion = true;
	}
	
	public void setOutputDir(File outputDir) {
		this.outDir = outputDir.getAbsolutePath() + "/";
	}
	
	public void setFilenameAppend(String append) {
		this.appendToFilename = append;
	}
	
	public void setStatusUpdatePreference(Context context, String preference) {
		this.context = context;
		this.statusUpdatePreference = preference;
	}
	
	public void setUploadOnCompletion(Context context, boolean upload) {
		this.context = context;
		this.uploadOnCompletion = upload;
	}
	
	public void setDeleteOnCompletion(boolean delete) {
		this.deleteOnCompletion = delete;
	}
	
	public static void zipFiles(File... files)
	{
		TaskZipper zipper = new TaskZipper();
		zipper.zipFiles("", "", files);
	}
	
	public File[] zipFiles(String outputDir, String append, File... files) {
		File inf = null;
		ArrayList<File> completed = new ArrayList<File>();
		// Compress files
		for (int i = 0; i < files.length; i++) {
			try {
				inf = files[i];
				publishProgress(new ZipperProgress(inf, "started"));
				BufferedInputStream ins = new BufferedInputStream(new FileInputStream(inf));
				File outf;
				if (outputDir.length() == 0) {
					outf = new File(files[i].getAbsolutePath() + append + ".zip");
				} else {
					outf = new File(outputDir + files[i].getName() + append + ".zip");
				}
				ZipOutputStream outs = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outf)));

				// Compress data
				ZipEntry entry = new ZipEntry(inf.getName());
				outs.putNextEntry(entry);
				byte data[] = new byte[ClassConsts.BUFFER_SIZE];
				int count;
				while ((count = ins.read(data, 0, ClassConsts.BUFFER_SIZE)) != -1) {
					outs.write(data, 0, count);
				}
				ins.close();
				outs.close();
				if (deleteOnCompletion) {
					// Remove uncompressed
					inf.delete();
				}
				Log.i(TAG, "Zipped " + outf.getName());
				publishProgress(new ZipperProgress(inf, "finished"));
				completed.add(outf);
			} catch (Exception e) {
				Log.e(TAG, "Zipper failed " + e.getMessage());
				if (inf != null) {
					publishProgress(new ZipperProgress(inf, "failed: " + e.getMessage()));
				}
			}
		}
		return completed.toArray(new File[completed.size()]);
	}

	@Override
	protected File[] doInBackground(File... files) {
		return zipFiles(outDir, appendToFilename, files);
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		if ((statusUpdatePreference.length() > 0) && (context != null)) {
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString(statusUpdatePreference, "Waiting to zip file(s)");
			editor.commit();
		}
	}
	
	@Override
	protected void onProgressUpdate(ZipperProgress... files) {
		super.onProgressUpdate(files);

		if ((statusUpdatePreference.length() > 0) && (context != null)) {
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
			SharedPreferences.Editor editor = settings.edit();
			for (int i = 0; i < files.length; i++) {
				editor.putString(statusUpdatePreference, files[i].toString());
			}
			editor.commit();
		}
	}
	
	@Override
	protected void onPostExecute(File[] result) {
		super.onPostExecute(result);
	}
}

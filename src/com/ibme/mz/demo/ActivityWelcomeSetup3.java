// Copyright (c) 2014, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Based on code by Maxim Osipov (https://github.com/maximosipov/actopsy)

package com.ibme.mz.demo;

import com.ibme.mz.demo.R;
import com.ibme.preference.AgePreference;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ActivityWelcomeSetup3 extends PreferenceActivity implements OnSharedPreferenceChangeListener {

	@SuppressWarnings("unused")
	private static final String TAG = ActivityWelcomeSetup3.class.getSimpleName();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferences_welcome_setup_3);
		
		final float scale = getResources().getDisplayMetrics().density;
		
        ListView v = getListView();
        
        TextView tv = new TextView(this);
        tv.setText("Next we would like to know a little bit more about you\n\nAll this information is optional but helps us understand your data\n\nPlease enter your details and press Next to continue");
        tv.setTextAppearance(this, android.R.style.TextAppearance_Medium);
        tv.setPadding((int)(10 * scale), (int)(30 * scale), (int)(10 * scale), 0);
        tv.setGravity(Gravity.CENTER);
        v.addHeaderView(tv);
        
        Button b = new Button(this);
        b.setText("Next");
        b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
        		Intent welcomeActivity = new Intent(getBaseContext(), ActivityWelcomeComplete.class);
        		startActivity(welcomeActivity);
        		finish();
            }
        });
        v.addFooterView(b);
        
        Preference pref;
        
    	pref = findPreference("listGender");
		pref.setSummary(((ListPreference)pref).getEntry());
		
		pref = findPreference("numericAge");
		pref.setSummary(((AgePreference)pref).getDisplayValue());
	}
	
	@Override
	public void onBackPressed() {
		Intent welcomeActivity = new Intent(getBaseContext(), ActivityWelcomeSetup1.class);
		startActivity(welcomeActivity);
		finish();
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		Preference pref = findPreference(key);
		if (key.equals("listGender")) {
			pref.setSummary(((ListPreference)pref).getEntry());
		} else if (key.equals("numericAge")) {
			pref.setSummary(((AgePreference)pref).getDisplayValue());
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}
}

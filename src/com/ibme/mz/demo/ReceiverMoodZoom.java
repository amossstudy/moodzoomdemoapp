// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Originally written by Maxim Osipov

package com.ibme.mz.demo;

import com.ibme.mz.demo.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

public class ReceiverMoodZoom extends BroadcastReceiver {

	private static final String TAG = ReceiverMoodZoom.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(TAG, "Mood Zoom alarm received");

		SharedPreferences schedule = context.getSharedPreferences(
		        ClassConsts.MOOD_ZOOM_SCHEDULE, Context.MODE_PRIVATE);
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
		
		long lMoodZoomType = schedule.getLong("typeMoodZoom", 0); // 0: disabled, 1: daily, 2: high intensity
		
		ClassMoodZoomPrompt moodZoomPrompt = new ClassMoodZoomPrompt(context);
		
		long ts = System.currentTimeMillis();
		moodZoomPrompt.init(ts);
		moodZoomPrompt.update(context, ts, lMoodZoomType);
		moodZoomPrompt.fini();
		
		Intent i = new Intent(context, ActivityMoodZoom.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.putExtra("typeMoodZoom", lMoodZoomType);
		context.startActivity(i);

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		ServiceCollect.CreateMoodZoomSchedule(context, prefs);
		ServiceCollect.ScheduleMoodZoom(context);
		
		if (lMoodZoomType > 0) {
			Resources res = context.getResources();
			
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
			
			mBuilder.setSmallIcon(R.drawable.ic_notification_bar);
			mBuilder.setContentTitle(res.getString(R.string.mood_zoom_notification_title));
			mBuilder.setContentText(res.getString(R.string.mood_zoom_notification_message));
			
			String defaultRingtone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString();
			
			String strUri = settings.getString("listNotificationSound", defaultRingtone);
			
			if (strUri == "") {
				mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
			} else {
				mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
				
				mBuilder.setSound(Uri.parse(strUri));
			}
			
			// The stack builder object will contain an artificial back stack for the
			// started Activity.
			// This ensures that navigating backward from the Activity leads out of
			// your application to the Home screen.
			TaskStackBuilder stackBuilder = TaskStackBuilder.from(context);
			// Adds the back stack for the Intent (but not the Intent itself)
			stackBuilder.addParentStack(ActivityMoodZoom.class);
			// Adds the Intent that starts the Activity to the top of the stack
			stackBuilder.addNextIntent(i);
			
			PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
			
			mBuilder.setContentIntent(resultPendingIntent);
			
			NotificationManager mNotificationManager =
			    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			
			mNotificationManager.notify(ClassConsts.NOTIFICATION_MOOD_ZOOM, mBuilder.getNotification());
		}
	}
}

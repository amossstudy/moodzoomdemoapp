// Copyright (c) 2014, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Based on code by Maxim Osipov (https://github.com/maximosipov/actopsy)

package com.ibme.mz.demo;

import com.ibme.mz.demo.R;

import com.actionbarsherlock.app.SherlockActivity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.Button;

public class ActivityWelcomeComplete extends SherlockActivity {

	@SuppressWarnings("unused")
	private static final String TAG = ActivityWelcomeComplete.class.getSimpleName();

	private int eventId;
	
//	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
	public void onCreate(Bundle savedInstanceState) {
//		if (BuildConfig.DEBUG) {
//			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//	                 .detectAll()
//	                 .penaltyLog()
//	                 .build());
//		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome_complete);

		// Handle close button
        final Button close = (Button) findViewById(R.id.buttonFinish);
        
        close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

        		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ActivityWelcomeComplete.this);
        		
        		SharedPreferences.Editor editor = prefs.edit();
        		editor.putLong("dateSetupCompleted", System.currentTimeMillis());
        		
        		editor.commit();
        		
        		Intent profileActivity = new Intent(getBaseContext(), ActivityProfile.class);
        		startActivity(profileActivity);
        		finish();
            }
        });
    	
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        eventId = Integer.parseInt(prefs.getString("listEvent", Integer.toString(getResources().getInteger(R.integer.event_none))));
    }

	@Override
	public void onBackPressed() {
		Intent welcomeActivity;
		if (eventId == getResources().getInteger(R.integer.event_bigspd)) {
			welcomeActivity = new Intent(getBaseContext(), ActivityWelcomeSetup3.class);
		} else {
			welcomeActivity = new Intent(getBaseContext(), ActivityWelcomeSetup4.class);
		}
		startActivity(welcomeActivity);
		finish();
	}
}
